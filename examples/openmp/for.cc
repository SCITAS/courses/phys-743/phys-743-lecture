#include <iostream>
#include <omp.h>

int main() {

#pragma omp parallel num_threads(2)
  {
    auto myrank = omp_get_thread_num();
#pragma omp for
    for (int i = 0; i < 6; ++i) {
      std::printf("Thread %i handling i=%i\n", myrank, i);
    }
  }

  return 0;
}
