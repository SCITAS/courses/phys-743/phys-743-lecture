#include <chrono>
#include <iostream>
#include <random>
#include <vector>

using clk = std::chrono::high_resolution_clock;
using millisecond = std::chrono::duration<double, std::milli>;

int main() {
  const int N = 1e8;

  std::vector<double> a(N), b(N), c(N);

  for (int i = 0; i < N; ++i) {
    a[i] = 1.0 * i;
    b[i] = 10.0 * i;
  }

  std::mt19937 gen(0);
  std::uniform_real_distribution<> rand(0, 1000);

  auto alpha = rand(gen);

  auto t1 = clk::now();
  for (int i = 0; i < N; ++i) {
    c[i] = a[i] + alpha * b[i];
  }
  auto t2 = clk::now();

  millisecond ms_double = t2 - t1;

  std::cout << "For loop ran in " << ms_double.count() << "ms" << std::endl;
  return 0;
}
