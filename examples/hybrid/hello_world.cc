#include <iostream>
#include <mpi.h>
#include <omp.h>

int main(int argc, char *argv[]) {
  int provided, size, rank, nthreads, tid;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_SINGLE, &provided);

  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  #pragma omp parallel default(shared) private(tid, nthreads)
  {
    nthreads = omp_get_num_threads();
    tid = omp_get_thread_num();
    std::printf("Hello from thread %i out of %i from process %i out of %i\n", tid, nthreads, rank, size);
  }
  MPI_Finalize();
  return 0;
}
