#include "print_size.hh"
#include <chrono>
#include <iomanip>
#include <iostream>
#include <mpi.h>
#include <vector>

void fill_buffer(std::vector<int> &buf, size_t size) {
  buf.resize(size);
  for (auto &v : buf) {
    v = 0;
  }
}

using clk = std::chrono::high_resolution_clock;
using second = std::chrono::duration<double>;

int main() {
  int prank, psize;
  const auto N{30};
  size_t REP{1000};

  MPI_Init(NULL, NULL);
  MPI_Comm_rank(MPI_COMM_WORLD, &prank);
  MPI_Comm_size(MPI_COMM_WORLD, &psize);

  std::vector<int> buf;
  auto partner = (prank + 1) % psize;

  for (size_t n = 0; n < N; n += 2) {
    auto size = 1 << n;
    fill_buffer(buf, size);

    auto t_start = clk::now();
    for (size_t repetition = 0; repetition < REP; ++repetition) {
      MPI_Send(buf.data(), buf.size(), MPI_INT, partner, 0, MPI_COMM_WORLD);
      MPI_Recv(buf.data(), buf.size(), MPI_INT, partner, 0, MPI_COMM_WORLD,
               MPI_STATUS_IGNORE);
    }
    auto time_s = (second{clk::now() - t_start}).count() / REP;

    if (prank == 0) {
      auto size_b = size * sizeof(int);
      std::cout << "PingPing"
                << " size: " << std::setw(10) << printHuman(size_b, "B", 2)
                << " time: " << std::setw(10) << printHuman(time_s, "s")
                << " bandwidth: " << std::setw(10)
                << printHuman(size_b / time_s, "B/s", 2) << std::endl;
    }
    if (size > 256)
      REP = 1;
  }

  MPI_Finalize();

  return 0;
}
