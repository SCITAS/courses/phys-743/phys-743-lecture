find_package(MPI REQUIRED COMPONENTS MPICXX)

foreach(tgt hello_mpi send_recv isend_recv ping_ping ping_pong datatypes pack_unpack)
  add_executable(${tgt} ${tgt}.cc)
  target_link_libraries(${tgt} PRIVATE ${MPI_CXX_LIBRARIES})
  target_include_directories(${tgt} PRIVATE ${MPI_CXX_INCLUDE_DIRS})
  target_compile_options(${tgt} PRIVATE ${MPI_CXX_COMPILE_OPTIONS})
endforeach()
