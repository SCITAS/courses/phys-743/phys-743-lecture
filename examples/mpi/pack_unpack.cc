#include <assert.h>
#include <iostream>
#include <mpi.h>
#include <vector>

void fill_buffer(std::vector<int> &buf) {
  for (auto &v : buf) {
    v = 0;
  }
}

int main() {
  int rank, size;
  std::vector<char> buf(100);
  int position{0}, count;
  MPI_Status status;

  int a;
  double d[10];

  MPI_Init(NULL, NULL);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  assert(size == 2 && "Works only with 2 procs");

  if (rank == 0) {
    a = 0xcafe;
    MPI_Pack(&a, 1, MPI_INT, buf.data(), buf.size(), &position, MPI_COMM_WORLD);
    MPI_Pack(d, 10, MPI_DOUBLE, buf.data(), buf.size(), &position,
             MPI_COMM_WORLD);
    MPI_Send(buf.data(), position, MPI_PACKED, 1, 0, MPI_COMM_WORLD);
  } else if (rank == 1) {
    MPI_Recv(buf.data(), buf.size(), MPI_PACKED, 0, 0, MPI_COMM_WORLD, &status);
    MPI_Unpack(buf.data(), buf.size(), &position, &a, 1, MPI_INT,
               MPI_COMM_WORLD);
    MPI_Unpack(buf.data(), buf.size(), &position, d, 10, MPI_DOUBLE,
               MPI_COMM_WORLD);
  }

  if (rank == 1) {
    MPI_Get_count(&status, MPI_PACKED, &count);
    std::cout << " position: " << position << " count: " << count << " - a: 0x"
              << std::hex << a << std::endl;
  }

  MPI_Finalize();

  return 0;
}
