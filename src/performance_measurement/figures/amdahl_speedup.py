import numpy as np
import matplotlib.pyplot as plt


def speedup(n: np.ndarray, parallel_frac: float) -> np.ndarray:
    return 1.0 / (1.0 - parallel_frac + parallel_frac / n)

n = np.array([2**i for i in range(11)])
parallel_fracs = np.array([0.5, 0.75, 0.9, 0.95, 0.99])
max_speedups = 1.0 / (1.0 - parallel_fracs)

fig, ax = plt.subplots(figsize=(10, 7))
ax.loglog(n, n, 'k--', alpha=0.4, label='Ideal scaling')
for i, (frac, max_speedup) in enumerate(zip(parallel_fracs, max_speedups)):
    ax.loglog(n, speedup(n, frac), label=f'p = {frac:.2f}')
    ax.text(900, 1.1*max_speedup,
            f'{max_speedup:.0f}',
            color=f'C{i}',
            ha='right',
            fontsize=11,
            fontweight='bold',
            )

ax.set_xlim([n[0], n[-1]])
ax.set_ylim([n[0], n[-1]])
ax.set_xlabel('Number of processors')
ax.set_ylabel('Speedup')
ax.set_title("Amdahl's law")

ticks = [1, 10, 100, 1000]
ax.set_xticks(ticks)
ax.set_xticklabels(ticks)
ax.set_yticks(ticks)
ax.set_yticklabels(ticks)

plt.legend()
plt.savefig('amdahl_speedup.pdf')
plt.show()
