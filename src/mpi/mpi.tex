\renewcommand{\FIGREP}{src/mpi/figures}

\section{Message Passing Interface (MPI)}
\label{sec:mpi}
\intersec{deneb}

\begin{frame}
	\frametitle{MPI}
	\framesubtitle{Goals of this section}
	\begin{itemize}
		\item Introduce distributed memory programming paradigm
		\item Point-to-point communications
		\item Collective communications
	\end{itemize}
\end{frame}

\subsection{Introduction}
\begin{frame}
	\frametitle{MPI}
	\framesubtitle{Overview and goals of MPI}

	\begin{itemize}
		\item MPI is a \textit{Message-Passing Interface} specification
		\item There are many implementations (MPICH, MVAPICH, Intel MPI, OpenMPI,
		      etc)
		\item Library interface, not a programming language
		\item It is standardized
		      \begin{itemize}
			      \item Defined by the \href{https://www.mpi-forum.org/}{MPI forum}
			      \item Current version is MPI 4.0
		      \end{itemize}
		\item As such, it is portable, flexible and efficient
		\item Interface to C and Fortran in standard
	\end{itemize}
\end{frame}

\subsection{MPI environment}
\begin{frame}[fragile]
	\frametitle{MPI}
	\framesubtitle{A simple hello world example}

	\cxxfile[%
		title={mpi/hello\_mpi.cc},
	]{examples/mpi/hello_mpi.cc}
\end{frame}

\note{
	\begin{itemize}
		\item MIMD paradigm
		\item Multiple process from the beginning
		\item Move the cout before init and after finalize
	\end{itemize}
}

\begin{frame}[b]
	\frametitle{Environment}
	\framesubtitle{}
	\begin{itemize}
		\item MPI code is bordered by a \cxxinline{MPI_Init} and a \cxxinline{MPI_Finalize}
		\item MPI starts $N$ processes numbered $0, 1, ..., N-1$. It is the process \textit{rank}
		\item They are grouped in a \textit{communicator} of \textit{size} $N$
		\item After init, MPI provides a default communicator called \code{MPI\_COMM\_WORLD}
	\end{itemize}
	\addimage[width=7cm]{\FIGREP/communicator}{4.5cm}{3.2cm}
\end{frame}

\begin{frame}[exercise, fragile]
	\frametitle{Hello world $\pi$}
	\begin{itemize}
		\item In the $pi$ code initialize/finalize properly MPI
		\item Print out the number of processes and the rank of each process
		\item Modify the makefile to use \code{mpicxx} instead of \code{g++}
		\item Write a batch script to run your parallel code
		      \begin{bashcode}
			      #!/bin/bash
			      #SBATCH -n <ntasks>
			      module purge
			      module load <compiler> <mpi library>
			      srun <my_mpi_executable>
		      \end{bashcode}

		      \emph{\textbf{Note :} To use MPI on the cluster you first have to load
			      a MPI implementation through the module \code{openmpi} or
			      \code{intel-oneapi-mpi}.}
	\end{itemize}
\end{frame}



\subsection{Terminology}

\begin{frame}
	\frametitle{Types of communications in MPI}
	\begin{itemize}
		\item {Point-to-Point (One-to-One)}
		\item {Collectives (One-to-All, All-to-One, All-to-All)}
		\item {One-sided/Shared memory (One-to...)}
		\item {Blocking and Non-Blocking of all types}
	\end{itemize}
\end{frame}

\subsection{Blocking point-to-point communications}

\begin{frame}[exercise]
	\frametitle{MPI}
	\framesubtitle{Message passing concepts}

	\begin{itemize}
		\item Let's derive a minimal message-passing interface
		\item The goal is to define an interface for \code{send} and \code{recv}
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Send/Receive}
	\framesubtitle{}

	\begin{cxxcode}{Syntax}
		int MPI_Ssend(const void *buf, int count, MPI_Datatype datatype, int dest,
		int tag, MPI_Comm comm);

		int MPI_Recv(void *buf, int count, MPI_Datatype datatype, int source,
		int tag, MPI_Comm comm, MPI_Status *status);
	\end{cxxcode}

	\begin{itemize}
		\item \code{buf} pointer to the data to send/receive
		\item \code{count} number of element to send/receive
		\item \code{datatype} datatype of the data to send/receive
		\item \code{dest}, \code{source} the rank of the destination/source of the communication
		\item \code{tag} a message tag to differentiate the communications
		\item \code{comm} communicator in which to communication happens
		\item \code{status} object containing information on the communication
	\end{itemize}
\end{frame}
\note{
	\begin{itemize}
		\item \url{https://www.mpi-forum.org/docs/mpi-4.0/mpi40-report.pdf}
		\item MPI\_Send on page 32
	\end{itemize}
}

\begin{frame}[fragile,b]
	\frametitle{Send/Receive}
	\framesubtitle{Details on the buffer}

	\addimage[width=7cm]{\FIGREP/buffer}{6.5cm}{3.5cm}

	\begin{itemize}
		\item Buffer is a pointer to the first data (\cxxinline{buf}), a size (\cxxinline{count}) and a \cxxinline{datatype}
		\item Datatypes (extract):
		      \begin{itemize}
			      \item \cxxinline{MPI_INT}
			      \item \cxxinline{MPI_UNSIGNED}
			      \item \cxxinline{MPI_FLOAT}
			      \item \cxxinline{MPI_DOUBLE}
		      \end{itemize}
		\item For \cxxinline{std::vector<double> vect}:
		      \begin{itemize}
			      \item \cxxinline{buf = vect.data()}
			      \item \cxxinline{count = vect.size()}
			      \item \cxxinline{datatype = MPI_DOUBLE}
		      \end{itemize}
	\end{itemize}
	\vspace{1cm}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Send/Receive}
	\framesubtitle{Useful constants and status}

	Constants:
	\begin{itemize}
		\item \cxxinline{MPI_STATUS_IGNORE} to state that the status is ignored
		\item \cxxinline{MPI_PROC_NULL} placeholder for the source or destination
		\item \cxxinline{MPI_ANY_SOURCE} is a wildcard for the source of a receive
		\item \cxxinline{MPI_ANY_TAG} is a wildcard for the tag of a receive
	\end{itemize}

	Status:
	\begin{itemize}
		\item Structure containing \cxxinline{tag} and \cxxinline{source}
		      \begin{cxxcode}{}
			      MPI_Status status;
			      std::cout << "Tag: " << status.tag << " - "
			      << "Source: " << status.source << std::endl;
		      \end{cxxcode}
		\item Size of the message can be asked using the status
		      \begin{cxxcode}{}
			      int MPI_Get_count(const MPI_Status *status, MPI_Datatype datatype,
			      int *count);
		      \end{cxxcode}

	\end{itemize}
\end{frame}


\begin{frame}
	\frametitle{Send/Receive}
	\framesubtitle{Example}

	\cxxfile[title={mpi/send\_recv.cc},
		minted options app={firstline=16,
				lastline=29}]{examples/mpi/send_recv.cc}
\end{frame}

\begin{frame}[exercise, fragile,t]
	\frametitle{Ring reduction of $\pi$}

	\begin{minipage}{0.45\textwidth}
		\begin{overprint}
			\only<1>{\includegraphics[width=\linewidth]{\FIGREP/ring_explanation_0}}
			\only<2>{\includegraphics[width=\linewidth]{\FIGREP/ring_explanation_1}}
			\only<3>{\includegraphics[width=\linewidth]{\FIGREP/ring_explanation_2}}
			\only<4>{\includegraphics[width=\linewidth]{\FIGREP/ring_explanation_3}}
			\only<5>{\includegraphics[width=\linewidth]{\FIGREP/ring_explanation_4}}
			\only<6>{\includegraphics[width=\linewidth]{\FIGREP/ring_explanation_5}}
		\end{overprint}
	\end{minipage}
	\hspace{.5cm}
	\begin{minipage}{0.45\textwidth}
		\begin{overprint}
			\onslide<1>
			\begin{itemize}
				\item \cxxinline{l_sum = local_computation();}\\
				      \cxxinline{sum += l_sum;}
			\end{itemize}

			\onslide<2>
			\begin{itemize}
				\item \cxxinline{send_buf = l_sum;}
			\end{itemize}

			\onslide<3>
			\begin{itemize}
				\item \cxxinline{send(send_buf);}\\
				      \cxxinline{receive(recv_buf);}
			\end{itemize}

			\onslide<4>
			\begin{itemize}
				\item \cxxinline{send_buf = recv_buf;}\\
				      \cxxinline{sum += recv_buf;}
			\end{itemize}

			\onslide<5>
			\begin{itemize}
				\item \cxxinline{send(send_buf);}\\
				      \cxxinline{receive(recv_buf);}
			\end{itemize}

			\onslide<6>
			\begin{itemize}
				\item Split the sum space between the processes
				\item Implement a ring to communicate the partial sum between the
				      processes. using \cxxinline{MPI\_Ssend} and
				      \cxxinline{MPI_Recv}

				      \emph{\textbf{Remember :} each MPI process runs the same code!}

				      \emph{\textbf{Note :} in a loop the next process is \cxxinline{(prank + 1) \% psize} and the previous is \cxxinline{(prank - 1 + psize) \% psize}}
			\end{itemize}
		\end{overprint}
	\end{minipage}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Send/Receive}
	\framesubtitle{Send variants}
	\begin{itemize}
		\item \cxxinline{MPI_Ssend} : (S for Synchronous) function returns when other end
		      posted matching recv and the buffer can be safely reused
		\item \cxxinline{MPI_Bsend} : (B for Buffer) function returns immediately, send
		      buffer can be reused immediately
		\item \cxxinline{MPI_Rsend} : (R for Ready) can be used only when a receive
		      is already posted
		\item \cxxinline{MPI_Send} : acts like \cxxinline{MPI_Bsend} on small
		      arrays, and like \cxxinline{MPI_Ssend} on bigger ones
	\end{itemize}
\end{frame}

\note{
	\begin{itemize}
		\item For Ssend the receive does not need to be finished but it has to be started
		\item For Bsend need to attach a buffer section 3.6 \cxxinline{MPI_Buffer_attach}
		\item Rsend is here for some case where hardware can avoid a hand-shake
		\item For Send the size of buffer is implementation dependent, usually can
		      be altered with an environment variable
	\end{itemize}
}

\begin{frame}[fragile]
	\frametitle{Send/Receive}
	\framesubtitle{Particularity of \cxxinline{MPI_Send}}

	\cxxfile[title={mpi/ping\_ping.cc},
		minted options app={firstline=31,
				lastline=40}]{examples/mpi/ping_ping.cc}

	\pause
	\begin{consoleoutput}
		$ mpirun -np 2 ./mpi/ping_ping
		PingPing size:       4.0B time:    303.5ns bandwidth:  12.6MiB/s
		PingPing size:      16.0B time:    291.0ns bandwidth:  52.4MiB/s
		PingPing size:      64.0B time:    289.7ns bandwidth: 210.7MiB/s
		PingPing size:     256.0B time:    327.9ns bandwidth: 744.5MiB/s
		PingPing size:     1.0KiB time:    686.9ns bandwidth:   1.4GiB/s
	\end{consoleoutput} %$
\end{frame}

\begin{frame}[fragile]
	\frametitle{Send/Receive}
	\framesubtitle{Combined send-receive}

	\begin{cxxcode}{Syntax}
		int MPI_Sendrecv(const void *sendbuf, int sendcount, MPI_Datatype sendtype,
		int dest, int sendtag,
		void *recvbuf, int recvcount, MPI_Datatype recvtype,
		int source, int recvtag,
		MPI_Comm comm, MPI_Status *status);
	\end{cxxcode}

	\begin{itemize}
		\item Combines a send and a receive, to help mitigate deadlocks
		\item Has a in-place variant \cxxinline{MPI_Sendrecv_replace}
	\end{itemize}
\end{frame}

\begin{frame}[exercise,fragile]
	\frametitle{Ring reduction of $\pi$}
	\framesubtitle{Using \code{MPI\_Sendrecv}}

	\begin{itemize}
		\item Modify the previous exercise to use \cxxinline{MPI_Sendrecv}
	\end{itemize}
\end{frame}

\subsection{Non-blocking point-to-point communications}

\begin{frame}[containsverbatim]
	\frametitle{Non-blocking send/receive}

	\begin{cxxcode}{Syntax}
		int MPI_Isend(const void *buf, int count, MPI_Datatype datatype, int dest,
		int tag, MPI_Comm comm, MPI_Request *request);

		int MPI_Irecv(void *buf, int count, MPI_Datatype datatype, int source,
		int tag, MPI_Comm comm, MPI_Request *request);
	\end{cxxcode}

	\begin{itemize}
		\item \code{I} for \emph{immediate}
		\item \cxxinline{request} in addition to parameters from blocking version
		\item receive does not have a status
		\item \cxxinline{request} is an object attached to the communication
		\item the communications starts but is not completed
		\item \code{S}, \code{B}, and \code{S} variant are also defined
	\end{itemize}
\end{frame}

\begin{frame}[containsverbatim]
	\frametitle{Non-blocking send/receive}
	\framesubtitle{Completion}

	\begin{cxxcode}{Syntax}
		int MPI_Wait(MPI_Request *request, MPI_Status *status);

		int MPI_Test(MPI_Request *request, int *flag, MPI_Status *status);
	\end{cxxcode}

	\begin{itemize}
		\item completion of communication should be checked
		\item \cxxinline{MPI_Test} or \cxxinline{MPI_Wait}
		\item send completed means the buffer can be reused
		\item receive completed means the buffer can be read
		\item \cxxinline{status} is set a completion
		\item \cxxinline{flag} is \code{true} if completed, \code{false} otherwise
	\end{itemize}
\end{frame}


\begin{frame}
	\frametitle{Send/Receive}
	\framesubtitle{Example}

	\cxxfile[title={mpi/sendrecv.cc},
		minted options app={firstline=22,
				lastline=31}]{examples/mpi/isend_recv.cc}
\end{frame}

\begin{frame}[exercise,fragile]
	\frametitle{Ring reduction of $\pi$}
	\framesubtitle{Using non-blocking send}

	\begin{itemize}
		\item Modify the previous exercise to use \cxxinline{MPI_Isend} and
		      \cxxinline{MPI_Recv}
		\item Do not forget to \code{wait}
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Non-blocking send/receive}
	\framesubtitle{Multiple completions}

	\begin{itemize}
		\item \cxxinline{MPI_Waitall}, \cxxinline{MPI_Testall} wait or test completion of all the pending requests
		\item \cxxinline{MPI_Waitany}, \cxxinline{MPI_Testany} wait or test completion of one out on many
		\item \cxxinline{MPI_Waitsome}, \cxxinline{MPI_Testsome} wait or test completion of all the enabled requests

		\item for arrays of statuses can use \cxxinline{MPI_STATUSES_IGNORE}
		\item \cxxinline{MPI_Request_get_status} equivalent to \cxxinline{MPI_Test} but does not free completed requests
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Probing}
	\framesubtitle{}

	\begin{cxxcode}{Syntax}
		int MPI_Iprobe(int source, int tag, MPI_Comm comm, int *flag,
		MPI_Status *status);

		int MPI_Probe(int source, int tag, MPI_Comm comm, MPI_Status *status);
	\end{cxxcode}

	\begin{itemize}
		\item check incoming messages without receiving
		\item Immediate variant returns \code{true} if matching message exists
	\end{itemize}
\end{frame}

\subsection{Collective communications}

\begin{frame}[fragile]
	\frametitle{Collective communications}
	\framesubtitle{Synchronization}

	\begin{cxxcode}{Syntax}
		int MPI_Barrier(MPI_Comm comm);
	\end{cxxcode}

	\begin{itemize}
		\item collective communications \textbf{must} be called by all processes in the communicator
		\item barrier is hard synchronization
		\item avoid as much a possible
	\end{itemize}
\end{frame}

\begin{frame}[fragile,t]
	\frametitle{Collective communications}
	\framesubtitle{Broadcast}

	\begin{cxxcode}{Syntax}
		int MPI_Bcast(void *buffer, int count, MPI_Datatype datatype, int root,
		MPI_Comm comm);
	\end{cxxcode}

	\begin{itemize}
		\item the \code{root} process sends data to every other process
	\end{itemize}

	\begin{center}
		\begin{overprint}
			\only<1>{\addimage[width=4cm]{\FIGREP/bcast0}{7cm}{1cm}}
			\only<2>{\addimage[width=4cm]{\FIGREP/bcast1}{7cm}{1cm}}
		\end{overprint}
	\end{center}
\end{frame}

\begin{frame}[fragile,t]
	\frametitle{Collective communications}
	\framesubtitle{Scatter}

	\begin{cxxcode}{Syntax}
		int MPI_Scatter(const void *sendbuf, int sendcount, MPI_Datatype sendtype,
		void *recvbuf, int recvcount, MPI_Datatype recvtype, int root,
		MPI_Comm comm);
	\end{cxxcode}

	\begin{itemize}
		\item the \code{root} process sends a piece of the data to all processes
		\item the \cxxinline{sendbuf}, \cxxinline{sendcount} and
		      \cxxinline{sendtype} are only relevant on the \code{root}
	\end{itemize}

	\begin{overprint}
		\only<1>{\addimage[width=4cm]{\FIGREP/scatter0}{7cm}{1cm}}
		\only<2>{\addimage[width=4cm]{\FIGREP/scatter1}{7cm}{1cm}}
	\end{overprint}
\end{frame}

\begin{frame}[fragile,t]
	\frametitle{Collective communications}
	\framesubtitle{Gather}

	\begin{cxxcode}{Syntax}
		int MPI_Gather(const void *sendbuf, int sendcount, MPI_Datatype sendtype,
		void *recvbuf, int recvcount, MPI_Datatype recvtype, int root,
		MPI_Comm comm);
	\end{cxxcode}

	\begin{itemize}
		\item all process their data to the \code{root} process
		\item the \cxxinline{recvbuf}, \cxxinline{recvcount} and
		      \cxxinline{recvtype} are only relevant on the \code{root}
		\item \cxxinline{recvcount} is the size per process not the total size
	\end{itemize}

	\begin{center}
		\begin{overprint}
			\only<1>{\addimage[width=4cm]{\FIGREP/gather0}{7cm}{1cm}}
			\only<2>{\addimage[width=4cm]{\FIGREP/gather1}{7cm}{1cm}}
		\end{overprint}
	\end{center}
\end{frame}

\begin{frame}[fragile,t]
	\frametitle{Collective communications}
	\framesubtitle{Gather to all}

	\begin{cxxcode}{Syntax}
		int MPI_Allgather(const void *sendbuf, int sendcount,
		MPI_Datatype sendtype, void *recvbuf, int recvcount,
		MPI_Datatype recvtype, MPI_Comm comm);
	\end{cxxcode}

	\begin{itemize}
		\item all process send their data to all other process
	\end{itemize}

	\begin{center}
		\begin{overprint}
			\only<1>{\addimage[width=4cm]{\FIGREP/gather0}{7cm}{1cm}}
			\only<2>{\addimage[width=4cm]{\FIGREP/allgather}{7cm}{1cm}}
		\end{overprint}
	\end{center}
\end{frame}

\begin{frame}[fragile,t]
	\frametitle{Collective communications}
	\framesubtitle{All to all gather/scatter}

	\begin{cxxcode}{Syntax}
		int MPI_Alltoall(const void *sendbuf, int sendcount, MPI_Datatype sendtype,
		void *recvbuf, int recvcount, MPI_Datatype recvtype,
		MPI_Comm comm);
	\end{cxxcode}

	\begin{itemize}
		\item all process send their a piece of their data to all other process
	\end{itemize}

	\begin{center}
		\begin{overprint}
			\only<1>{\addimage[width=4cm]{\FIGREP/alltoall0}{7cm}{1cm}}
			\only<2>{\addimage[width=4cm]{\FIGREP/alltoall1}{7cm}{1cm}}
		\end{overprint}
	\end{center}
\end{frame}

\begin{frame}[exercise,fragile]
	\frametitle{Ring reduction of $\pi$}
	\framesubtitle{Using collective communications}

	\begin{itemize}
		\item \cxxinline{MPI_Gather} the
		      partial sums to the root process.
		\item \cxxinline{MPI_Bcast} the total sum all the process
	\end{itemize}
\end{frame}

\begin{frame}[fragile,t]
	\frametitle{Collective communications}
	\framesubtitle{Reduction}

	\begin{cxxcode}{Syntax}
		int MPI_Reduce(const void *sendbuf, void *recvbuf, int count,
		MPI_Datatype datatype, MPI_Op op, int root, MPI_Comm comm);
	\end{cxxcode}

	\begin{itemize}
		\item data from all process are reduced on the \code{root} process
		\item common operations being \cxxinline{MPI_SUM}, \cxxinline{MPI_MAX},
		      \cxxinline{MPI_MIN}, \cxxinline{MPI_PROD}
		\item a \cxxinline{MPI_Allreduce} variant exists where all the process have the results
		\item \cxxinline{MPI_IN_PLACE} can be passed in the \cxxinline{sendbuf} of
		      \code{root} for a \emph{reduce} a of all process for a
		      \emph{allreduce}
	\end{itemize}


	\begin{center}
		\begin{overprint}
			\only<1>{\addimage[width=4cm]{\FIGREP/reduction0}{7cm}{1cm}}
			\only<2>{\addimage[width=4cm]{\FIGREP/reduction1}{7cm}{1cm}}
			\only<3>{\addimage[width=4cm]{\FIGREP/reduction2}{7cm}{1cm}}
		\end{overprint}
	\end{center}
\end{frame}

\begin{frame}[exercise,fragile]
	\frametitle{Ring reduction of $\pi$}
	\framesubtitle{Using collective communications}

	\begin{itemize}
		\item Modify the previous exercise to use \cxxinline{MPI_Reduce} and
		      \cxxinline{MPI_Bcast}
		\item Modify it again to use \cxxinline{MPI_Allreduce}
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Collective communications}
	\framesubtitle{Partial reductions}

	\begin{cxxcode}{Syntax}
		int MPI_Scan(const void *sendbuf, void *recvbuf, int count,
		MPI_Datatype datatype, MPI_Op op, MPI_Comm comm);

		int MPI_Exscan(const void *sendbuf, void *recvbuf, int count,
		MPI_Datatype datatype, MPI_Op op, MPI_Comm comm);
	\end{cxxcode}

	\begin{itemize}
		\item performs the prefix reduction on data
		\item \cxxinline{MPI_Scan} on process $i$ contains the reduction of values
		      from processes $[0, i]$
		\item \cxxinline{MPI_Exscan} on process $i$ contains the reduction of values
		      from processes $[0, i[$
		\item \cxxinline{MPI_IN_PLACE} can be passed a \cxxinline{sendbuf}
	\end{itemize}
\end{frame}

\begin{frame}[fragile,t]
	\frametitle{Parallelization of the poisson code}

	\begin{minipage}{.45\linewidth}
		\centering
		\begin{overprint}
			\only<1>{\includegraphics[width=.8\linewidth]{\FIGREP/grid_0}}
			\only<2>{\includegraphics[width=.8\linewidth]{\FIGREP/grid_1}}
			\only<3>{\includegraphics[width=.8\linewidth]{\FIGREP/grid_2}}
			\only<4>{\includegraphics[width=.8\linewidth]{\FIGREP/grid_3}}
			\only<5>{\includegraphics[width=.8\linewidth]{\FIGREP/grid_4}}
			\only<6>{\includegraphics[width=.8\linewidth]{\FIGREP/grid_4}}
		\end{overprint}
	\end{minipage}
	\begin{minipage}{.45\linewidth}
		\begin{overprint}
			\onslide<1>
			\begin{itemize}
				\item Parallelize the Poisson 2D problem using the Messages Passing
				      Interface (MPI)
			\end{itemize}

			\onslide<2>
			\begin{itemize}
				\item The memory allocation is done in the C default manner, “Row-Major
				      Order”: make your domain decomposition by lines
			\end{itemize}

			\onslide<3>
			\begin{itemize}
				\item $p$ domains of size $N/p$ each (1 per process)
			\end{itemize}

			\onslide<4>
			\begin{itemize}
				\item Adding \emph{ghost} lines before and after
			\end{itemize}

			\onslide<5>
			\begin{itemize}
				\item Use the \emph{ghost} lines to receive the missing local data
			\end{itemize}

			\onslide<6>
			\begin{itemize}
				\item Start using \cxxinline{MPI_Sendrecv} to implement the communications
				\item You can use the number of iteration as a check
				\item Remove the \cxxinline{dump()} function to start
				\item Once it is working try to use \emph{non-blocking} communications
			\end{itemize}
		\end{overprint}
	\end{minipage}
\end{frame}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../phys_743_parallel_programming"
%%% End:
